// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config({});
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import compression from 'compression';
import morgan from 'morgan';
import indexRoute from './routes';
const app = express();
app.use(morgan('dev'));
app.use(
  bodyParser.urlencoded({
    extended: false,
  }),
);
app.use(bodyParser.json());
app.use(cors());
app.use(compression());

app.use('/api', indexRoute);

app.use(function errorHandler(err, req, res, next) {
  res.status(400);
  res.json({ message: err.message });
});

export default app;
