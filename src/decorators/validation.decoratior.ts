/* eslint-disable prefer-rest-params */
import { ObjectSchema } from 'joi';

export type ValidationType = 'body' | 'params' | 'query';

export const UseValidation = (
  type: ValidationType,
  schema: ObjectSchema,
): MethodDecorator => {
  return function (target, key, descriptior: any) {
    const oldValue = descriptior.value;
    descriptior.value = async function () {
      const [req, res] = arguments;

      const payload = await schema
        .validateAsync(req[type], {
          stripUnknown: true,
        })
        .catch((error) => {
          res.status(400).json({
            message: error.details[0].message,
          });
        });

      req[type] = payload;

      const result = oldValue.apply(this, arguments);

      return result;
    };
    return descriptior;
  };
};
