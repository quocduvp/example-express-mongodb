describe('mongoose', () => {
  test('Check mongodb url', () => {
    expect(process.env.MONGO_URL).toBeDefined();
  });
});
