import mongoose from 'mongoose';
let conn = null;
// Prints "MongoServerError: bad auth Authentication failed."
export const startMongoose = async () => {
  try {
    if (conn == null) {
      conn = mongoose
        .connect(process.env.MONGO_URL, {
          serverSelectionTimeoutMS: 5000,
        })
        .then(() => mongoose);
      await conn;
    }
    return conn;
  } catch (error) {
    console.error(error);
    throw error;
  }
};
