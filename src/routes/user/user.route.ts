import { Router } from 'express';
import userController from './user.controller';

const userRoute = Router();

userRoute.get('/', userController.list);
userRoute.post('/', userController.create);

export default userRoute;
