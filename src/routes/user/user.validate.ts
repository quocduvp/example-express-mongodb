import Joi from 'joi';

export const CreateUserSchema = Joi.object().keys({
  users: Joi.array().items(
    Joi.object().keys({
      _id: Joi.string(),
      username: Joi.string().required(),
      email: Joi.string().email().required(),
      birthdate: Joi.string().required(),
    }),
  ),
});

export const ListUserSchema = Joi.object().keys({
  name: Joi.string().allow(null, ''),
});
