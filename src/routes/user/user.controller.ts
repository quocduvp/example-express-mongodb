import { NextFunction, Request, Response } from 'express';
import { UseValidation } from '../../decorators/validation.decoratior';
import userService from './user.service';
import { CreateUserSchema, ListUserSchema } from './user.validate';

class UserController {
  static instance: UserController;

  static getInstance() {
    if (!UserController.instance) {
      UserController.instance = new UserController();
    }
    return UserController.instance;
  }

  @UseValidation('body', CreateUserSchema)
  async create(req: Request, res: Response, next: NextFunction) {
    try {
      const result = await userService.create(req.body.users);
      return res.json({
        users: result,
      });
    } catch (error) {
      next(error);
    }
  }

  @UseValidation('query', ListUserSchema)
  async list(req: Request, res: Response, next: NextFunction) {
    try {
      const { name } = req.query;
      const result = await userService.list(name as string);
      return res.json({
        users: result,
      });
    } catch (error) {
      next(error);
    }
  }
}

export default UserController.getInstance();
