import { model, Schema } from 'mongoose';

export interface User {
  _id: Schema.Types.ObjectId;
  username: string;
  email: string;
  birthdate: Date;
}

const UserSchema = new Schema<User>(
  {
    username: String,
    email: { type: String },
    birthdate: { type: Date },
  },
  { timestamps: true },
);

export const UserModel = model<User>('users', UserSchema);
