import { User, UserModel } from './user.model';

class UserService {
  static instance: UserService;

  static getInstance() {
    if (!UserService.instance) {
      UserService.instance = new UserService();
    }
    return UserService.instance;
  }

  async create(users: User[]) {
    try {
      const results = [];
      for await (const user of users) {
        const filterById = user._id
          ? {
              _id: {
                $ne: user._id,
              },
            }
          : {};
        const [cond1, cond2] = await Promise.all([
          UserModel.countDocuments({
            ...filterById,
            username: user.username,
          }),
          UserModel.countDocuments({
            ...filterById,
            email: user.email,
          }),
        ]);
        if (cond1 + cond2 > 0) {
          throw new Error('Username or email already exists.');
        }
        let result = await UserModel.findOneAndUpdate(
          {
            _id: user._id,
          },
          {
            username: user.username,
            email: user.email,
            birthdate: user.birthdate,
          },
          {
            new: true,
          },
        );
        if (!result) {
          result = await UserModel.create(user);
        }
        results.push(result);
      }
      return results;
    } catch (error) {
      throw error;
    }
  }

  async list(name: string) {
    try {
      const filter = {};
      if (name || name !== '') {
        filter['$or'] = [
          {
            username: new RegExp(name, 'gi'),
          },
          {
            email: new RegExp(name, 'gi'),
          },
        ];
      }
      const result = await UserModel.find(
        filter,
        '_id username email birthdate',
        {
          sort: 'createdAt',
        },
      );
      return result;
    } catch (error) {
      throw error;
    }
  }
}

export default UserService.getInstance();
