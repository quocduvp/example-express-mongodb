import { Router } from 'express';
import userRoute from './user/user.route';

const route = Router();

route.use('/user', userRoute);

export default route;
