/* eslint-disable @typescript-eslint/no-var-requires */
const serverless = require('serverless-http');
const app = require('./dist');
const { startMongoose } = require('./dist/connectors/mongoose.connect');
const handler = serverless(app.default);
module.exports.handler = async (event, context) => {
  await startMongoose();
  const result = await handler(event, context);
  return result;
};
