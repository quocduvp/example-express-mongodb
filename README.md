# Example demo

[API URL](https://funqslocu4.execute-api.ap-southeast-1.amazonaws.com/dev/)

[POSTMAN ENV](user_app_env.postman_environment.json)
[POSTMAN API COLLECTION](user_app_api.postman_collection.json)

# Setup Environment variables (run local)

- MONGO_URL

# Setup Environment variables (gitlab ci)

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- MONGO_URL

Start server local
```sh
npm install -g serverless
npm i
npm run dev
```